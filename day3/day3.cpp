#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>
#include "day3.hpp"

FabrickSquare::FabrickSquare()
    : myClaimedAreas()
    , myClaims()
    , myWidth(0)
    , myHeight(0)
{
}

void FabrickSquare::recordClaim(std::string recievedClaim)
{
    claim incomingClaim;
    std::string token;
    size_t previous, current = 0;
    recievedClaim.erase(remove_if(recievedClaim.begin(), recievedClaim.end(), isspace), recievedClaim.end());
    previous = recievedClaim.find("#") + 1;

    current = recievedClaim.find("@");
    incomingClaim.id = std::stoi(recievedClaim.substr(previous, current - previous));

    previous = current + 1;
    current = recievedClaim.find(",");
    incomingClaim.distFromLeft = std::stoi(recievedClaim.substr(previous, current - previous));
    previous = current + 1;
    current = recievedClaim.find(":");
    incomingClaim.distFromTop = std::stoi(recievedClaim.substr(previous, current - previous));
    previous = current + 1;
    current = recievedClaim.find("x");
    incomingClaim.width = std::stoi(recievedClaim.substr(previous, current - previous));
    previous = current + 1;
    current = recievedClaim.size();
    incomingClaim.height = std::stoi(recievedClaim.substr(previous, current - previous));
    myClaims.push_back(incomingClaim);

    int claimWidth, claimHeight = 0;
    claimWidth = incomingClaim.distFromLeft + incomingClaim.width;
    claimHeight = incomingClaim.distFromTop + incomingClaim.height;

    if(claimWidth > myWidth)
    {
        myWidth = claimWidth;
        std::cout << "New max width: " << claimWidth << "\n";
    }
    if(claimHeight > myHeight)
    {
        myHeight = claimHeight;
        std::cout << "New max height: " << claimHeight << "\n";
    }
}

void FabrickSquare::constructSquare()
{
    myClaimedAreas.resize(myWidth * myHeight);
}

void FabrickSquare::executeClaims()
{
    for(auto const& it : myClaims)
    {
        claimArea(it.distFromLeft, it.distFromTop, it.width, it.height);
    }
}

void FabrickSquare::claimArea(int fromLeft, int fromTop, int width, int height)
{
    const int offset = fromTop * myWidth + fromLeft;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            size_t index = offset + i * myWidth + j;
            myClaimedAreas[index]++;
        }
    }
}

void FabrickSquare::printClaims()
{
    for (int i = 0; i < myHeight; i++)
    {
        for (int j = 0; j < myWidth; j++)
        {
            std::cout << myClaimedAreas[i*myWidth+j];
        }
        std::cout << "\n";
    }
}

int FabrickSquare::countOverlap()
{
    size_t count;
    for (auto const& it : myClaimedAreas)
    {
        if (it > 1)
        {
            count++;
        }
    }
    return count;
}

int FabrickSquare::evaluateClaims()
{
    for (auto const& it : myClaims)
    {
        if (isClaimUnique(it.distFromLeft, it.distFromTop, it.width, it.height))
        {
            return it.id;
        }
    }
    return 0;
}

bool FabrickSquare::isClaimUnique(const int fromLeft, const int fromTop, const int width, const int height)
{
    const int offset = fromTop * myWidth + fromLeft;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            size_t index = offset + i * myWidth + j;
            if (myClaimedAreas[index] != 1)
            {
                return false;
            }
        }
    }
    return true;
}

int main()
{
    FabrickSquare myFabrickSquare;
    std::ifstream input("input.txt");

    while(1)
    {
        char line[256];

        input.getline(line, 256);
        if (input.eof())
            break;
        std::string claim(line);
        myFabrickSquare.recordClaim(claim);
    }
    myFabrickSquare.constructSquare();
    myFabrickSquare.executeClaims();

    std::ofstream out("out.txt");
    std::streambuf *coutbuf = std::cout.rdbuf();
    std::cout.rdbuf(out.rdbuf());
    myFabrickSquare.printClaims();
    std::cout.rdbuf(coutbuf);

    std::cout << "The number of overlapping squares is " << myFabrickSquare.countOverlap() << "\n";

    std::cout << "The only unique claim is: " << myFabrickSquare.evaluateClaims() << "\n";
    return 0;
}
