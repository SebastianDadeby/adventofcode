#include <vector>

class FabrickSquare
{
public:
    FabrickSquare();
    void recordClaim(const std::string recievedClaim);
    void constructSquare();
    void executeClaims();
    void printClaims();
    int countOverlap();
    int evaluateClaims();

private:
    void claimArea(int fromLeft, int fromTop, int width, int height);
    bool isClaimUnique(const int fromLeft, const int fromTop, const int width, const int height);

    struct claim
    {
        int id;
        int distFromLeft;
        int distFromTop;
        int width;
        int height;
    };

    std::vector<int>    myClaimedAreas;
    std::vector<claim>  myClaims;
    int                 myWidth;
    int                 myHeight;

};
