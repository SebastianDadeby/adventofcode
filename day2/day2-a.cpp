#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <algorithm>

int main()
{
    std::ifstream input("input.txt");
    int twoChar = 0;
    int threeChar = 0;
    while(1)
    {
        std::string boxID;
        int occurences[26];
        memset(occurences, 0, 26*sizeof(int));
        input >> boxID;
        if (input.eof())
            break;

        for (char& c : boxID)
        {
            std::cout << c << " converted to " << (int(c)-97) << " ";
            occurences[((int)c)-97]++;
        }
        std::cout << "\n";
        if(std::find(occurences, occurences + 25, 2) != occurences + 25)
        {
            ++twoChar;
        }
        if(std::find(occurences, occurences + 25, 3) != occurences + 25)
        {
            ++threeChar;
        }
        for (int i= 0; i<25; i++)
        {
            std::cout << occurences[i] << " ";
        }
        std::cout << "\n";
    }

    std::cout << twoChar << " doubles and " << threeChar << " tripples\n";
    std::cout << "result is " << twoChar * threeChar << "\n";
}
