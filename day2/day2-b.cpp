#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <algorithm>
#include <cmath>
#include <vector>

int distance(int a[], int b[], int size)
{
    int result = 0;
    for (int i = 0; i < size; i++)
    {
        result += abs(a[i] - b[i]);
        //std::cout << a[i] << " - " << b[i] << " =+ " << result << "\n";
    }
    //std::cout << "distance: " << result << "\n";
    return result;
}

int main()
{
    std::ifstream input("input.txt");
    int twoChar = 0;
    int threeChar = 0;
    std::vector<std::string> idVect;
    std::string resultA;
    std::string resultB;

    while(1)
    {
        std::string boxID;

        input >> boxID;
        idVect.push_back(boxID);
        if (input.eof())
            break;
    }

    for (int i = 0; i<idVect.size() - 1; i++)
    {
        std::string boxIDA = idVect[i];

        std::cout << "string 1: " << boxIDA << "\n";

        for (int j = i + 1; j < idVect.size() - 1; j++)
        {
            int match = boxIDA.length();
            std::string boxIDB = idVect[j];
            for (int k = 0; k < boxIDA.length(); k++)
            {
                if (boxIDA[k] != boxIDB[k])
                {
                    --match;
                    if (match < boxIDA.length() - 2)
                    {
                        break;
                    }
                }
            }
            if (match == boxIDA.length() - 1)
            {
                resultA = boxIDA;
                resultB = boxIDB;
                goto finish;
            }
            std::cout << "string 2: " << boxIDB << "\n";
        }
    }
finish:

    std::cout << resultA << "\n";
    std::cout << resultB << "\n";
}
