#include <iostream>
#include <fstream>
#include <vector>

int main()
{
    int result = 0;
    std::vector<int> freqList;
    std::ifstream input("input.txt");
    while(!input.eof())
    {
        int receive = 0;
        input >> receive;
        result += receive;
        if(std::find(freqList.begin(), freqList.end(), result) != freqList.end())
        {
            break;
        }
        freqList.push_back(result);
    }
    std::cout << "first double frequency is " << result;
    return 0;
}
