#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

int main()
{
    int result = 0;
    std::vector<int> freqList;
    std::vector<int> inputList;
    std::ifstream input("input.txt");
    while(1)
    {
        int receive = 0;
        input >> receive;
        if(input.eof())
        {
            break;
        }
        inputList.push_back(receive);
    }
    std::vector<int>::iterator it = inputList.begin();
    while(1)
    {
        if (it != inputList.end())
        {
            result += *it;
            it++;

            if(std::find(freqList.begin(), freqList.end(), result) != freqList.end())
            {
                //std::cout << "double frequency found \n";
                break;
            }
            freqList.push_back(result);
        }
        else
        {
            it = inputList.begin();
        }

    }
    std::cout << "first double frequency is " << result << " after " << freqList.size() << " tries \n";
    return 0;
}
