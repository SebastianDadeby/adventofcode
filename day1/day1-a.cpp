#include <iostream>
#include <fstream>

int main()
{
    int result = 0;
    std::ifstream input("input.txt");
    while(!input.eof())
    {
        int receive = 0;
        input >> receive;
        result += receive;
    }
    std::cout << "result is " << result << "\n";

    return 0;
}
