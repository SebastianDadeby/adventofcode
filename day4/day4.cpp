#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>
#include <iterator>
#include <numeric>

#include "day4.hpp"

int main()
{
    std::ifstream input("input.txt");
    GuardLog guardRecord;

    while(1)
    {
        char line[256];

        input.getline(line, 256);
        if (input.eof())
        {
            break;
        }

        std::string inputLine(line);
        guardRecord.parseLogEntry(inputLine);
    }

    guardRecord.sort();
    guardRecord.parseLog();

    std::cout << "Part 1 result: \n";
    int resultId = guardRecord.getGuardMostSlept();
    int resultMinute = guardRecord.getGuardMinuteMostSlept(resultId);
    std::cout << resultId << " sleeps the most, most like to be asleep at 00:" << resultMinute << "\n";
    std::cout << "The result (Guard Id * most likely minute) is " << resultId * resultMinute << "\n";

    std::cout << "Part 2 result: \n";
    resultId = guardRecord.getGuardMostLikelySleep();
    std::cout << resultId << " is most likely to sleep" << std::endl;
    resultMinute = guardRecord.getGuardMinuteMostSlept(resultId);

    std::cout << "at minute " << resultMinute << "\n";
    std::cout << "The result (Guard Id * most likely minute) is " << resultId * resultMinute << "\n";
}

void GuardLog::newActiveGuard(const int id)
{
    auto const& it = std::find_if(myGuards.begin(), myGuards.end(), [&id](const std::pair<int, Guard*>& element){return element.first == id;});
    if(it == myGuards.end())
    {
        Guard* newGuard = new Guard(id);
        myGuards.push_back(std::make_pair(id, newGuard));
        myActiveGuard = newGuard;
    }
    else
    {
        myActiveGuard = it->second;
    }
}

void GuardLog::parseLog()
{
    for (auto it = myGuardLog.begin(); it != myGuardLog.end(); ++it)
    {
        if (it->action.compare(0,5,"Guard") == 0)
        {
            size_t idStart = it->action.find("#") + 1;
            size_t idLen = it->action.find(" ", idStart) - idStart;
            int guardId = stoi(it->action.substr(idStart, idLen));
            newActiveGuard(guardId);
        }
        else
        {
            myActiveGuard->sleepPeriod(it->minute, (it+1)->minute);
            ++it;   // We already covered the following awake statement above
        }
    }
}

void GuardLog::parseLogEntry(const std::string input)
{
    logEntry incomingEntry;

    incomingEntry.year = stoi(input.substr(1, 4));
    incomingEntry.month = stoi(input.substr(6, 2));
    incomingEntry.day = stoi(input.substr(9, 2));
    incomingEntry.hour = stoi(input.substr(12, 2));
    incomingEntry.minute = stoi(input.substr(15, 2));

    incomingEntry.action = input.substr(19, std::string::npos);

    myGuardLog.push_back(incomingEntry);
}

int GuardLog::getGuardMostSlept()
{
    int maxSlept = 0;
    int guardId = 0;
    for (auto const& it : myGuards)
    {
        int timeSlept = it.second->getMinutesSlept();
        if (timeSlept > maxSlept)
        {
            maxSlept = timeSlept;
            guardId = it.first;
        }
    }
    std::cout << "Guard " << guardId << " slept the most, total of " << maxSlept << " minutes" << std::endl;
    return guardId;
}

int GuardLog::getGuardMostLikelySleep()
{
    int timeSlept, minute, maxTimeSlept = 0, maxTimeSleptId = 0;
    for (auto const& it : myGuards)
    {
        minute = it.second->getMostLikelyMinuteAsleep();
        timeSlept = it.second->getTimeSleptAtMinute(minute);
        if (timeSlept > maxTimeSlept)
        {
            maxTimeSlept = timeSlept;
            maxTimeSleptId = it.first;
        }
    }
    return maxTimeSleptId;
}

int GuardLog::getGuardMinuteMostSlept(const int id)
{
    auto const& it = std::find_if(myGuards.begin(), myGuards.end(), [&id](const std::pair<int, Guard*>& element){return element.first == id;});
    return it->second->getMostLikelyMinuteAsleep();
}

void GuardLog::sort()
{
    std::sort(myGuardLog.begin(), myGuardLog.end(), eventSort);
}

void GuardLog::print()
{
    for (auto const& it : myGuardLog)
    {
        std::cout << it << "\n";
    }
}

GuardLog::GuardLog()
    : myGuardLog()
    , myGuards()
    , myActiveGuard(NULL)
{
}

Guard::Guard(int id)
    : myId(id)
    , myMinutesAsleep()
{
    myMinutesAsleep.resize(60);
}

int Guard::getMinutesSlept()
{
    return std::accumulate(myMinutesAsleep.begin(), myMinutesAsleep.end(), 0);
}

int Guard::getId()
{
    return myId;
}

void Guard::sleepPeriod(const int start, const int end)
{
    for(int i = start; i < end; i++)
    {
        myMinutesAsleep[i]++;
    }
}

int Guard::getMostLikelyMinuteAsleep()
{
    auto it = std::max_element(myMinutesAsleep.begin(), myMinutesAsleep.end());
    return std::distance(myMinutesAsleep.begin(), it);
}

int Guard::getTimeSleptAtMinute(const int minute)
{
    return myMinutesAsleep[minute];
}
