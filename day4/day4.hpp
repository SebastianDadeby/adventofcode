#include <vector>
#include <ostream>
#include <utility>

struct logEntry
{
    int         year;
    int         month;
    int         day;
    int         hour;
    int         minute;
    std::string action;
};

class Guard
{
public:
    Guard(int id);
    int getId();
    void sleepPeriod(const int start, const int end);
    int getMinutesSlept();
    int getMostLikelyMinuteAsleep();
    int getTimeSleptAtMinute(const int minute);

private:
    int myId;
    std::vector<int> myMinutesAsleep;
};

class GuardLog
{
public:
    GuardLog();
    void parseLogEntry(const std::string input);
    void parseLog();
    void sort();
    void print();
    int getGuardMostSlept();
    int getGuardMinuteMostSlept(const int id);
    int getGuardMostLikelySleep();

private:
    void newActiveGuard(const int id);

    std::vector<logEntry>         myGuardLog;
    std::vector<std::pair<int,Guard*>> myGuards;
    Guard*                        myActiveGuard;

};

std::ostream& operator<<(std::ostream& os, const logEntry& entry)
{
    os << entry.year << "-" << entry.month << "-" << entry.day << " " << entry.hour << ":" << entry.minute << " " << entry.action;
    return os;
};

bool eventSort(logEntry a, logEntry b)
{
    if (a.year > b.year)
    {
        return false;
    }
    else if(a.year < b.year)
    {
        return true;
    }

    if (a.month > b.month)
    {
        return false;
    }
    else if(a.month < b.month)
    {
        return true;
    }

    if (a.day > b.day)
    {
        return false;
    }
    else if(a.day < b.day)
    {
        return true;
    }

    if (a.hour > b.hour)
    {
        return false;
    }
    else if(a.hour < b.hour)
    {
        return true;
    }

    if (a.minute > b.minute)
    {
        return false;
    }
    else if(a.minute < b.minute)
    {
        return true;
    }

    return false; //Event are equal, no reason to change
}
