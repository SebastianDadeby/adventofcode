#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>


std::string performReactions(std::string inputStr)
{
    char nextChar, currentChar;
    bool changed = false;
    for (uint i = 0; i < inputStr.size(); i++)
    {
        nextChar = inputStr[i+1];
        currentChar = inputStr[i];
        if (abs((int)currentChar - (int)nextChar) == 32)
        {
            changed = true;
            inputStr.erase(i,2); // Remove current and subsequent chars from result
        }
        else
        {
            // Do nothing
        }
    }
    if (changed)
    {
        inputStr = performReactions(inputStr);
    }
    return inputStr;
}

int findShortestWithReduction(std::string inputStr)
{
    uint shortestLength = inputStr.size();
    char shortestChar = 0;
    std::string workStr = inputStr;
    for (int i = 65; i <= 90; i++)
    {
        char lowerCase = (int)i;
        char upperCase = (int)(i + 32);

        workStr.erase(std::remove(workStr.begin(), workStr.end(), lowerCase), workStr.end());
        workStr.erase(std::remove(workStr.begin(), workStr.end(), upperCase), workStr.end());

        workStr = performReactions(workStr);

        if (workStr.size() < shortestLength)
        {
            shortestLength = workStr.size();
            shortestChar = lowerCase;
            std::cout << "New shortest string by removing " << shortestChar << " is " << shortestLength << std::endl;
        }
        workStr = inputStr;
    }
    std::cout << "Shortest result comes from removing " << shortestChar << std::endl;
    return shortestLength;
}

int main()
{
    std::ifstream input("input.txt");
    std::string incomingString;
    std::string resultString;
    int resultSize;
    while(1)
    {
        std::string inputLine;
        if (input.eof())
        {
            break;
        }

        std::getline(input, inputLine);
        incomingString = incomingString + inputLine;
    }

    std::cout << incomingString.size() << std::endl;
    resultString = performReactions(incomingString);
    std::cout << resultString.size() << std::endl;
    resultSize = findShortestWithReduction(incomingString);
    std::cout << "Shortest string from removing one pair is " << resultSize << std::endl;
}

